import {Teachers} from './teachers';

export class Schoolgroups {
  id: number;
  peoplesamount: number;
  groupname: string;
  teachers: Teachers[];
}

