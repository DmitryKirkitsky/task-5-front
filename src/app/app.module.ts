import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EditSchoolgroupsComponent } from './edit-schoolgroups/edit-schoolgroups.component';
import { AddSchoolgroupsComponent } from './add-schoolgroups/add-schoolgroups.component';
import { SchoolgroupsComponent } from './schoolgroups/schoolgroups.component';
import { NavigationComponent } from './navigation/navigation.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {
    path: 'schoolgroups',
    component: SchoolgroupsComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'schoolgroups'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    EditSchoolgroupsComponent,
    AddSchoolgroupsComponent,
    SchoolgroupsComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
