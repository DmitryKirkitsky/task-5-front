import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchoolgroupsComponent } from './add-schoolgroups.component';

describe('AddSchoolgroupsComponent', () => {
  let component: AddSchoolgroupsComponent;
  let fixture: ComponentFixture<AddSchoolgroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSchoolgroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchoolgroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
