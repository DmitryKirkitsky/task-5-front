import { TestBed } from '@angular/core/testing';

import { SchoolgroupsService } from './schoolgroups.service';

describe('SchoolgroupsService', () => {
  let service: SchoolgroupsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolgroupsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
