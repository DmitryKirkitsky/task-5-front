import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Schoolgroups} from '../model/schoolgroups';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SchoolgroupsService {

  readonly SCHOOLGROUPS_URL = 'http://localhost:8080/schoolgroups';
  readonly FILTER_URL = 'http://localhost:8080/schoolgroups/filter';

  constructor(private http: HttpClient, private router: Router) { }

  getSchoolgroups(): Observable<Schoolgroups[]> {
    return this.http.get<Schoolgroups[]>(this.SCHOOLGROUPS_URL);
  }


  getSchoolgroupsById(id: number): Observable<Schoolgroups> {
    return this.http.get<Schoolgroups>(this.SCHOOLGROUPS_URL + '/' + id);
  }

  saveSchoolgroups(schoolgroups: Schoolgroups): Observable<any> {
    return this.http.post(this.SCHOOLGROUPS_URL, schoolgroups);
  }

  removeSchoolgroups(id: number): Observable<any> {
    return this.http.delete(this.SCHOOLGROUPS_URL + '/' + id);
  }

  updateSchoolgroups(schoolgroups: Schoolgroups, id: number): Observable<any> {
    return this.http.put(this.SCHOOLGROUPS_URL + '/' + id, schoolgroups);
  }

  filterSchoolgroups(id: number): Observable<any> {
    return this.http.get(this.FILTER_URL + '?schoolgroupsId=' + id);
  }
}
