import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Teachers} from '../model/teachers';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {
  readonly TEACHERS_URL = 'http://localhost:8080/teachers';

  constructor(private http: HttpClient, private router: Router) { }

  getTeachers(): Observable<Teachers[]> {
    return this.http.get<Teachers[]>(this.TEACHERS_URL);
  }
}
