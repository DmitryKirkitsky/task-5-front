import { Component, OnInit } from '@angular/core';
import {Schoolgroups} from '../model/schoolgroups';
import {Teachers} from '../model/teachers';
import {SchoolgroupsService} from '../service/schoolgroups.service';
import {TeachersService} from '../service/teachers.service';

@Component({
  selector: 'app-schoolgroups',
  templateUrl: './schoolgroups.component.html',
  styleUrls: ['./schoolgroups.component.css']
})
export class SchoolgroupsComponent implements OnInit {

  constructor(private schoolgroupsService: SchoolgroupsService, private teachersService: TeachersService) { }
  schoolgroups: Schoolgroups[];
  schoolgroup: Schoolgroups = new Schoolgroups();
  teachers: Teachers[];



  ngOnInit(): void {
    this.schoolgroupsService.getSchoolgroups().subscribe(
      (schoolgroups) => {
        this.schoolgroups = schoolgroups;
        console.log(schoolgroups);
      },
      err => {
        alert('error');
      }
    );
    this.teachersService.getTeachers().subscribe(
      (teachers) => {
        this.teachers = teachers;
        console.log(teachers);
      },
      err => {
        alert('error');
      }
    );
  }

}
