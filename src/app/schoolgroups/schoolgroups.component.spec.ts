import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolgroupsComponent } from './schoolgroups.component';

describe('SchoolgroupsComponent', () => {
  let component: SchoolgroupsComponent;
  let fixture: ComponentFixture<SchoolgroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchoolgroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolgroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
