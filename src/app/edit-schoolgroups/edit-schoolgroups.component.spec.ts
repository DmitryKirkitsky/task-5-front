import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSchoolgroupsComponent } from './edit-schoolgroups.component';

describe('EditSchoolgroupsComponent', () => {
  let component: EditSchoolgroupsComponent;
  let fixture: ComponentFixture<EditSchoolgroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSchoolgroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSchoolgroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
